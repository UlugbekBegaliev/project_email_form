package com.example.project_gns.controllers;

import com.example.project_gns.models.ConfirmationAccess;
import com.example.project_gns.models.User;
import com.example.project_gns.repositories.ConfirmationAccessRepository;
import com.example.project_gns.repositories.UserRepository;
import com.example.project_gns.services.EmailSenderService;
import lombok.Data;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Data
@Controller
public class UserController {

    private UserRepository userRepository;
    private ConfirmationAccessRepository confirmationAccessRepository;

    private EmailSenderService emailSenderService;

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public ModelAndView displayRegistration(ModelAndView modelAndView, User user) {
        modelAndView.addObject("user", user);
        modelAndView.setViewName("register");
        return modelAndView;
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ModelAndView registerUser(ModelAndView modelAndView, User user) {

        User existingUser = userRepository.findByEmailIdIgnoreCase(user.getEmailId());
        if (existingUser != null) {
            modelAndView.addObject("message", "Email уже существует!");
            modelAndView.setViewName("error");
        } else {
            userRepository.save(user);

            ConfirmationAccess confirmationAccess = new ConfirmationAccess(user);

            confirmationAccessRepository.save(confirmationAccess);

            SimpleMailMessage mailMessage = new SimpleMailMessage();
            mailMessage.setTo(user.getEmailId());
            mailMessage.setSubject("Завершить регистрацию!");
            mailMessage.setFrom("uluga.begaliev@gmail.com");
            mailMessage.setText("Чтобы подтвердить свою учетную запись, нажмите здесь: " +
                    "http://localhost:8082/confirm-account?access=" +
                    confirmationAccess.getAccess());

            emailSenderService.sendEmail(mailMessage);

            modelAndView.addObject("emailId", user.getEmailId());

            modelAndView.setViewName("successfulRegistration");
        }

        return modelAndView;
    }

    @RequestMapping(value = "/confirm-account", method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView confirmUserAccount(ModelAndView modelAndView,
                                           @RequestParam("access") String confirmationAccess) {

        ConfirmationAccess access = confirmationAccessRepository.findByConfirmationAccess(confirmationAccess);

        if (access != null) {
            User user = userRepository.findByEmailIdIgnoreCase(access.getUser().getEmailId());
            user.setIsEnabled(true);
            userRepository.save(user);
            modelAndView.setViewName("accountVerified");
        } else {
            modelAndView.addObject("message", "Ссылка недействительна!");
            modelAndView.setViewName("error");
        }

        return modelAndView;
    }

}
