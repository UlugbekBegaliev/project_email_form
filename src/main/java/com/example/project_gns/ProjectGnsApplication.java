package com.example.project_gns;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectGnsApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProjectGnsApplication.class, args);
    }

}
