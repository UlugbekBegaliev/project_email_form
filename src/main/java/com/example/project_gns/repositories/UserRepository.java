package com.example.project_gns.repositories;


import com.example.project_gns.models.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, String> {

    User findByEmailIdIgnoreCase(String emailId);
}
