package com.example.project_gns.repositories;

import com.example.project_gns.models.ConfirmationAccess;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ConfirmationAccessRepository extends CrudRepository<ConfirmationAccess, String> {

    ConfirmationAccess findByConfirmationAccess(String confirmationAccess);
}
