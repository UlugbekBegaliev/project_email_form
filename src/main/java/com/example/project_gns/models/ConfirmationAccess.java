package com.example.project_gns.models;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Data
@Entity
@Table(name = "confirmation_access")
public class ConfirmationAccess {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "access_id")
    private Long accessId;

    @Column(name = "access")
    private String access;

    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;

    @OneToOne(targetEntity = User.class, fetch = FetchType.EAGER)
    @JoinColumn(nullable = false, name = "user_id")
    private User user;

    public ConfirmationAccess(){
    }

    public ConfirmationAccess(User user){
        this.user = user;
        createdDate = new Date();
        access = UUID.randomUUID().toString();
    }
}
